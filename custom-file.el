(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-names-vector
   ["#212526" "#ff4b4b" "#b4fa70" "#fce94f" "#729fcf" "#e090d7" "#8cc4ff" "#eeeeec"])
 '(custom-safe-themes
   '("061eb0d383d6534d6e36978bc6822976d34e2df5c100e273c7d1732e92ff66ee" "d299567a414e4c170ec4bb76e961d4849ba20d56656b627b12e521e478b4799c" "b142bbad7dc7427ea515575c105905d7eb9e16a103c74b9cedbb2966ef78c96c" "76625d2fb13d20c77db17c18c3f21c577d06d7991bb00331b6963c28914b905f" "f5ed00ed94fb0e4090dea45b61c5906040fdbf0ebd7a7d5f9caa33fd1797d19a" "3451cce144f406def98312f3c17a6657513730f5b63518daa53528941e657539" "2b7f6df023d2c9a89984b8eb996b71dc386af22fd56083ac3dbac248d111ebb5" "b7d0ac458ea60b1e565c383a740563b6c25cf6cc8b186e124671f8e6e79a8ad0" "8bec58da5bf4a16238f11cbc4a96dd9963db85f900fb998641b7cc1beabf218a" "775b7d3c39665a9ffb01b3d04872b7cac80945dc20982ca9b6fc7b1acbd31e12" "6be25ba666d234141106a2bc8af22a89de772ec729600dc81b63eb1da78134af" default))
 '(package-selected-packages
   '(elfeed pdf-outline-minor-mode pdf-tools lsp-haskell lsp-mode org-mode dired-hide-dotfiles evil-collection evil-colleciton ivy-hydra hydra evil helpful ivy-rich autothemer counsel ivy use-package)))

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
(put 'dired-find-alternate-file 'disabled nil)
