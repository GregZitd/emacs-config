(require 'autothemer)

(autothemer-deftheme
 greg "GregZi's custom theme"

 ((((class color) (min-colors #xFFFFFF))) ;; We are only concerned with graphical Emacs

  ;; Define our color palette
  (greg-bgk                 "#1c1c1c")
  (greg-mainfont            "#e8d0b2")
  (greg-blue                "#30638e")
  (greg-lightpink           "#e8aded")
  (greg-pink                "#ea59bc")
  (greg-green               "#699e3c")
  (greg-orange              "#ea6c59")
  (greg-yellow              "#eab559")
  (greg-grey                "#7896bc")
  (greg-purple              "#a243a5")
  (greg-black               "#000000")
  (greg-redish              "#e81e61")

  (greg-brown               "#8c6023")

  
  (greg-lightsteelblue      "#b0c4de")
  (greg-green2              "#5486c4")
  
  
  (greg-red                 "#d31923")
  
  (greg-matchcolor          "#ea59bc")
  )

 ;; Customize faces
 ((default                  (:foreground greg-mainfont :background greg-bgk :height 120))
  (cursor                   (:background greg-pink))
  (region                   (:background greg-lightpink :foreground greg-black))
  (minibuffer-prompt        (:foreground greg-redish))
  (button                   (:foreground greg-redish :underline t))
  (custom-link              (:foreground greg-redish :underline t))
  (font-lock-comment-face   (:foreground greg-grey))
  (font-lock-string-face    (:foreground greg-yellow :slant 'italic))
  (font-lock-keyword-face   (:foreground greg-orange))
  (font-lock-constant-face  (:foreground greg-green :weight 'bold))
  (font-lock-builtin-face   (:foreground greg-purple))

  ;;Ivy
  (ivy-current-match        (:foreground greg-black :background greg-lightpink))
  (ivy-subdir               (:foreground greg-yellow :weight 'bold))
  (ivy-minibuffer-match-face-1 ())
  (ivy-minibuffer-match-face-2 (:background greg-matchcolor :foreground greg-black))
  (ivy-minibuffer-match-face-3 (:background greg-matchcolor :foreground greg-black))
  (ivy-minibuffer-match-face-4 (:background greg-matchcolor :foreground greg-black))
  (counsel-key-binding      (:foreground greg-orange))
  (ivy-highlight-face       ())
  (ivy-cursor               (:foreground greg-pink))

  ;;Swiper
  (swiper-background-match-face-1 (:foreground greg-lightpink :slant 'italic))
  (swiper-background-match-face-2 (:foreground greg-matchcolor :slant 'italic))
  (swiper-background-match-face-3 (:foreground greg-matchcolor :slant 'italic))
  (swiper-background-match-face-4 (:foreground greg-matchcolor :slant 'italic))
  (swiper-line-face               ())
  (swiper-match-face-1            (:background greg-lightpink :foreground greg-black))
  (swiper-match-face-2            (:background greg-matchcolor :foreground greg-black))
  (swiper-match-face-3            (:background greg-matchcolor :foreground greg-black))
  (swiper-match-face-4            (:background greg-matchcolor :foreground greg-black))
  
  ;;Dired
  (dired-directory                (:foreground greg-yellow :weight 'bold))
  (dired-header                   (:foreground greg-redish))
  (dired-ignored                  (:foreground greg-grey))
  (dired-marked                   (:foreground greg-black :background greg-lightpink))
  (dired-mark                     (:foreground greg-purple))
  (dired-flagged                  (:foreground greg-red :slant 'italic))
  (direg-perm-write               (:foreground greg-green))
  
  ;;Haskell
  (haskell-constructor-face       (:foreground greg-mainfont))
  (haskell-operator-face          (:foreground greg-purple))
  (haskell-type-face              (:foreground greg-blue))
  
  ))
  

(provide-theme 'greg)
