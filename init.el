(setq inhibit-startup-message t)

(set-scroll-bar-mode nil) ;Disable visible scrollbar
(tool-bar-mode -1) ;Disable the toolbar
(tooltip-mode -1) ;Disable tooltips
(set-fringe-mode 10) ;Give some breathing room
(menu-bar-mode -1) ;Disable the menu bar


;; Initialize package sources
(require 'package)

(setq package-archives '(("melpa" . "https://melpa.org/packages/")
			 ("org" . "https://orgmode.org/elpa/")
			 ("elpa" . "https://elpa.gnu.org/packages/")))

;; Load and activate packages. Setting package-enable-at-startup to nil to
;; prevent packages accidentaly loading twice, as it is recommended in the
;; package-initialize documentation.
(package-initialize)
(setq package-enable-at-startup nil)

;; In case of a fresh install, gather the available packages for the above
;; specified package arcives
(unless package-archive-contents
  (package-refresh-contents))

;; Do not put 'customize' config in init.el; give it another file.
(setq custom-file "~/.emacs.d/custom-file.el")

;; Assuming that the code in custom-file is execute before the code
;; ahead of this line is not a safe assumption. So load this file
;; proactively.

(load-file custom-file)
;; Initialize use-package - Used for configuring and loadig other packages
;; If use-package is not installed on the system, install it
(unless (package-installed-p 'use-package)
  (package-install 'use-package))

(require 'use-package)

;; Whenever we load a package with use-package, ensure that it is already
;; downloaded on the system. If not download it.
(setq use-package-always-ensure t)

(use-package evil
  :init
  (setq evil-want-C-u-scroll t)
  ;; The following two lines are necessary for the package evil-collection
  (setq evil-want-integration t)
;;  (setq evil-want-keybinding nil)
  :config
  (evil-mode 1)
  (evil-global-set-key 'motion (kbd "/") 'swiper-isearch)
  (evil-global-set-key 'insert (kbd "C-h") 'evil-delete-backward-char-and-join)
  )


(use-package hydra)

;; Initialize Ivy - Completion framework for the minibuffer
(use-package ivy
  :bind
  (("C-x x" . ivy-resume)
   :map ivy-minibuffer-map
	(("C-j" . ivy-next-line)
	 ("C-k" . ivy-previous-line)
	 ("C-h" . ivy-backward-delete-char)
	 ("C-l" . ivy-alt-done)
	 ("C-i" . ivy-immediate-done)
	 ("C-d" . ivy-scroll-down-command)
	 ("C-u" . ivy-scroll-up-command)
	 ("C-o" . ivy-dispatching-done)
	 ("<escape>" . minibuffer-keyboard-quit))
   :map ivy-switch-buffer-map
         ("C-k" . ivy-previous-line))
  :config
  (ivy-mode 1)
  (setq ivy-count-format "(%d/%d) ")
  (setq ivy-wrap t)
  (setq confirm-nonexistent-file-or-buffer t)
  (setq ivy-height 7)
  )

;; Initialize Counsle - Extending Ivy functionality to common emacs functions
(use-package counsel
  :after ivy
  :bind
  ("C-s" . 'swiper-isearch)
  :config
  (counsel-mode 1))

(use-package ivy-hydra
  :after hydra)

;; Initialize ivy-rich - Display aditional information in the minibuffer when using Ivy
(use-package ivy-rich
  :after ivy
  :init
  (ivy-rich-mode 1))

;; Initialize helpful - Better documentation for the Emacs help system
(use-package helpful
  :custom
  (counsel-describe-function-function #'helpful-callable)
  (counsel-describe-variable-function #'helpful-variable)
  :bind
  (([remap describe-key] . helpful-key))
  :config
  (add-hook 'helpful-mode-hook
	  (lambda ()
	    (define-key evil-normal-state-local-map
	      (kbd "q") 'kill-buffer-and-window))))

;; Initialize dired - File management system for Emacs
(use-package dired
  :ensure nil
  :config
  (evil-define-key '(normal motion) dired-mode-map
    "j" 'dired-next-line
    "k" 'dired-previous-line
    ;; Setting l and h to go down and up a directory without opening a new buffer
    "l" 'dired-find-alternate-file
    "h" (lambda () (interactive) (find-alternate-file ".."))
    )
  (setq dired-listing-switches "-aghol")
  )
    
(use-package dired-hide-dotfiles
  :hook (dired-mode . dired-hide-dotfiles-mode)
  :config
  (evil-define-key '(normal motion) dired-mode-map
    (kbd "H") 'dired-hide-dotfiles-mode))

;; Initialize autothemer - Friendlier interface for making custom themes
(use-package autothemer)
(load-file "~/.emacs.d/greg-theme.el")
(load-theme 'greg t)

;; emacs can automatically create backup files. this tells emacs to put all backups in
;; ~/.emacs.d/backups. more info:
;; http://www.gnu.org/software/emacs/manual/html_node/elisp/Backup-Files.html
backup-directory-alist `(("." . ,(concat user-emacs-directory "backups")))

;; Set up lsp mode
(use-package lsp-mode
  :init
  (setq lsp-keymap-prefix "C-l"))

(use-package lsp-haskell)

(add-hook 'haskell-mode-hook #'lsp)
(add-hook 'haskell-literate-mode-hook #'lsp)

;; Initialize pdf-tools
(use-package pdf-tools
  :init
  (pdf-tools-install)
  (setq pdf-view-resize-factor 1.1)
  ;;Load the pdf-outline file so that emacs wont complain about not finding "pdf-outlin-buffer-mode-map" on init.
  (load-file "/home/greg/.emacs.d/elpa/pdf-tools-20200512.1524/pdf-outline.el")
  :bind
  (:map pdf-view-mode-map
	("j" . pdf-view-scroll-up-or-next-page)
	("k" . pdf-view-scroll-down-or-previous-page)
	("J" . pdf-view-next-line-or-next-page)
	("K" . pdf-view-previous-line-or-previous-page)
	("C-j" . pdf-view-next-page-command)
	("C-k" . pdf-view-previous-page-command)
   :map pdf-outline-buffer-mode-map
        ("C-j" . 'outline-forward-same-level)	
	("C-k" . 'outline-backward-same-level)
	("j" . 'next-line)
	("k" . 'previous-line)
	("l" . 'pdf-outline-follow-link-and-quit)
	("C-l" . 'pdf-outline-follow-link)
	("L" . pdf-outline-display-link)
 )) 

(use-package elfeed
  :init
  (global-set-key (kbd "C-x w") 'elfeed)
  (evil-set-initial-state 'elfeed-search-mode 'emacs)
  :bind
  (:map elfeed-search-mode-map
	("j" . next-line)
	("k" . previous-line)
	("l" . elfeed-search-show-entry))
  :config
  (setq elfeed-feeds
	'(("https://spreadprivacy.com/rss/" Privacy SoftwareIUse)
	  ("https://tutanota.com/blog/feed.xmal" Privacy SowtwareIUse)
	  ("https://www.getmonero.org/feed.xml" CryptoCurrency))))

;; Remapping basic keybindings that are not specific to one package
;; (global-set-key (kbd "<escape>") 'keyboard-quit)
(global-set-key (kbd "C-x f") 'counsel-find-file)
(global-set-key (kbd "C-x C-f") 'find-file-other-frame)

(global-set-key (kbd "C-q") 'kill-current-buffer)

(global-set-key (kbd "C-x C-b") 'switch-to-buffer-other-frame)
(global-set-key (kbd "C-x i") 'ibuffer)
